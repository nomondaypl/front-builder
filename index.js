const app = require('@nomondaypl/front-core').app;
const Crawler = require("crawler");
const viewsFolder = './views/default/';
const fs = require('fs-extra');
const notifier = require('node-notifier');

const config = require('./package.json');
let title;
let AppPort = process.env.PORT || 3030;

app.set('port', process.env.PORT || 3030);
const server = app.listen(app.get('port'), () => {
    console.log(`Aplikacja nasłuchuje na porcie ${server.address().port}`);
    const c = new Crawler({
        callback: function (error, res, done) {
            if (error) {
                console.log(error);

                notifier.notify({
                    title: 'Błąd!',
                    message: 'Wystąpił błąd podaczas generowania statycznych plików, sprawdź konsolę',
                    sound: true
                });


            } else {
                var $ = res.$;
                // $ is Cheerio by default
                var subpagename = $("head title").text();
                subpagename = subpagename.replace(/\s/g, '');
                subpagename = subpagename.slice(0, 0 - config.name.length);
                subpagename = subpagename + '.html';
                fs.writeFile('./static/' + subpagename, res.body, function (err) {
                    if (err)
                        throw err;
                    console.log('Strona ' + subpagename + ' została zapisana');
                });
                notifier.notify({
                    title: 'NoMonday FrontStarter',
                    message: 'Strona ' + subpagename + ' została zapisana',
                    sound: true
                });

            }

            server.close();
            done();
        }
    });

    fs.readdirSync(viewsFolder).forEach(file => {
        if (file === 'layouts' || file.substr(file.length - 4) !== '.hbs') {
            return;
        }
        title = file.slice(0, -4);
        c.queue('http://localhost:' + AppPort + '/' + title);

    });

//Kopiowanie publicznych elementów do statycznej aplikacji

    fs.copy('./public/layout/default/dist/css/', './static/layout/default/dist/css/', err => {
        if (err)
            return console.error(err);
        console.log('Skopiowano style');
    });

    fs.copy('./public/layout/default/dist/js/', './static/layout/default/dist/js/', err => {
        if (err)
            return console.error(err);
        console.log('Skopiowano javascript');
    });

    fs.copy('./public/layout/default/dist/gfx', './static/layout/default/dist/gfx', err => {
        if (err)
            return console.error(err);
        console.log('Skopiowano grafiki');
    });

    fs.copy('./public/layout/default/dist/fonts', './static/layout/default/dist/fonts', err => {
        if (err)
            return console.error(err);
        console.log('Skopiowano fonty');
    });
});
