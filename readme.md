# NoMonday Front-Starter

## Zastrzeżenia
To repozytorium jest własnocią NoMonday Sp. z o.o. i jako takie podlega ochronie. Nieautoryzowane korzystanie, udostępnianie, aktualizacja i tym podobne jest ściśle zakazane. Każdy przypadek naruszenia będzie ścigany z całą stanowczością.
Kontakt: hello@nomonday.pl


## Wprowadzenie
Rozszerzenie pakietu aplikacji Front-starter.
Kontakt deweloperski: `amadeusz.blanik@nomonday.pl`

## Instalacja
`npm i` lub `yarn install` w zależności od preferencji

## Korzystanie

* Alias package.json
`npm run build` lub `yarn build`

* Node 
`node index.js`

## TO-DO
Usunięcie niepotrzebnych zależnych oraz przebudowanie na klasę aby łatwiej można wywoływać kod z zewnątrz.
